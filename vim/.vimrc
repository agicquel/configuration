" VIM Configuration

" Annule la compatibilite avec l’ancetre Vi : totalement indispensable
set nocompatible

" Activation de pathogen
call pathogen#infect()

" Activation de NERDTree au lancement de vim
autocmd vimenter * NERDTree

" Active la coloration syntaxique
syntax enable

" Active les comportements specifiques aux types de fichiers comme
" la syntaxe et l’indentation
filetype on
filetype plugin on
filetype indent on

" -- Affichage
set title
set number
set ruler
set wrap
set scrolloff=3

" -- Recherche
set ignorecase
set smartcase
set incsearch
set hlsearch

" -- Beep
set visualbell
set noerrorbells " Empeche Vim de beeper

" Active le comportement ’habituel’ de la touche retour en arriere
set backspace=indent,eol,start

" Cache les fichiers lors de l’ouverture d’autres fichiers
set hidden

" Utilise la version sombre de Solarized
let g:hybrid_use_Xresources = 1
" let g:solarized_termcolors=256
set background=dark
colorscheme hybrid

" Les ; sont rarement utilises l’un a la suite de l’autre
:imap ;; <Esc>
:map ;; <Esc>

" -- Recherche
set ignorecase " Ignore la casse lors d’une recherche
set smartcase " Si une recherche contient une majuscule,
" re-active la sensibilite a la casse
set incsearch
" Surligne les resultats de recherche pendant la
" saisie
set hlsearch
" Surligne les resultats de recherche
"

